const db_url = "https://raw.githubusercontent.com/mocchapi/pesterchum-themes/main/db.json";

const template = `
<div class="item-main">
    <div class="item-top">
        <h3 class="item-name"><a href="#item-{name}">{name}</a></h3>
        <img class="item-icon" src="{icon}" alt="Compatible with the {client} client.">
    </div>

    <div class=item-bot>
        <p>by <b class="item-author">{author}</b></p>
        <p class="item-version">version {version}</p>
    </div>
    
    <p class="item-description">{description}</p>

    <p class="item-inherits">⚠️ Requires <a href="#item-{inherits}">{inherits}</a></p>
    <p>Last updated: {updatedDateString}</p>
</div>

<div class="item-sub">
    <a href="{download}" class="button item-download" download>Download</a>
    <a class="button button-less item-source" href="{source}" title="Source code">&lt;\/&gt;</a>
</div>
`
let db = {};
let itemcontainer

function toDate(timestamp) {
    console.log(timestamp)
    let dateFormat = new Date(timestamp*1000);
    return dateFormat.toLocaleString()
}

function hasEntry(name) {
    for (const item of db['entries']) {
        if (item['name'] == name) {return true;}
    }
    return false;
}

function onSearchChanged(box) {
    let searchTerm = box.value.toLowerCase();
    console.log('search for ' + searchTerm);

    for (const item of db['entries']) {
        console.log(item['name'])
        let vis = ((item['name'].toLowerCase().includes(searchTerm) || item['description'].toLowerCase().includes(searchTerm) || item['author'].toLowerCase().includes(searchTerm) || searchTerm == ""));

        let already_hidden = item['EL'].className.includes('hidden')


        if (!vis) {
            if (!already_hidden) {
                item['EL'].classList.add('hidden');
                console.log("hiding "+item['name']);
            }
        } else {
            if (already_hidden) {
                item['EL'].classList.remove('hidden');
                console.log("unhiding "+item['name']);
            }
        }
        console.log()
    }
    
}

function add_item(item) {
    // Build new theme card
    console.log("Building item "+item['name'])
    console.log(item)
    let node = document.createElement('li');
    node.className = "item";
    node.id = 'item-' + item['name']
    let out = template;
    for (const [key, value] of Object.entries(item)) {
        // Set each {variable} to the correct value
        out = out.replaceAll('{' + key + '}', value);
    }


    // Remove source button if there is no source linked
    node.innerHTML = out;
    if (item['source'] == "") {
        console.log("No source listed, removing button.")
        let el = node.getElementsByClassName("item-source")[0]
        console.log(el)
        el.parentElement.removeChild(el)
    }

    // Remove the "requires {inheits}" warning if it does not inherit
    if (item['inherits'] == "" || !hasEntry(item['inherits'])) {
        console.log("Removing dependency notice (empty or not in db)")
        let el = node.getElementsByClassName("item-inherits")[0]
        el.parentElement.removeChild(el)
    }

    itemcontainer.appendChild(node);
    console.log()
    return node;
}

async function get_db() {
    const response = await fetch(db_url);
    const jsonData = await response.json();
    return jsonData;
}

window.onload = async function () {
    itemcontainer = document.getElementById('itemcontainer')
    db = await get_db();

    for (const item of db['entries']) {
        item['updatedDateString'] = toDate(item['updated_at'])
        if (item['icon'] == '') {
            item['icon'] = 'pesterchum.png';
        }
        // Turn urls into actual <a> tags (https://stackoverflow.com/a/19484755)
        item['description'] = item['description'].replace(/(?:(https?\:\/\/[^\s]+))/m, '<a href="$1">$1</a>');

        // Create item
        item['EL'] = add_item(item);
    }


    if (location.hash) {
        let requested_hash = location.hash.slice(1);
        location.hash = '';
        location.hash = requested_hash;
    }
}